﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Threading;

namespace CopyTool
{
    public partial class CopyTool : Form
    {
        public CopyTool()
        {
            InitializeComponent();
            textBoxDestination.AllowDrop = true;
            listBoxSource.AllowDrop = true;
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            progressBar1.Step = 1;
            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = listBoxSource.Items.Count;

            foreach (string file in listBoxSource.Items)
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    string md5Source = GetMd5Hash(md5Hash, file);
                    string dest = textBoxDestination.Text.ToString() + Path.GetFileName(file);

                    if (!Directory.Exists(Path.GetDirectoryName(dest)))
                        Directory.CreateDirectory(Path.GetDirectoryName(dest));

                    File.Copy(file, dest, true);

                    string md5Dest = GetMd5Hash(md5Hash, file);

                    if (md5Source == md5Dest)
                    {
                        progressBar1.PerformStep();
                    }
                    else
                        MessageBox.Show("MD5 hash doesn't match");
                }
            }
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("X2"));
            }

            return sBuilder.ToString();
        }

        private void CopyTool_Load(object sender, EventArgs e)
        {

        }

        private void buttonBrowse1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in ofd.FileNames)
                {
                    listBoxSource.Items.Add(file);
                }
            }
        }

        private void buttonBrowse2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
                textBoxDestination.Text = fbd.SelectedPath.ToString();
        }

        private void textBoxDestination_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string filePath in files)
            {
                textBoxDestination.Text = filePath.ToString();
            }
        }

        private void textBoxDestination_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        private void listBoxSource_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string filePath in files)
                {
                    listBoxSource.Items.Add(filePath.ToString());
                }
            }
        }

        private void listBoxSource_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(listBoxSource);
            selectedItems = listBoxSource.SelectedItems;

            for (int i = selectedItems.Count; i > 0; i--)
            {
                listBoxSource.Items.Remove(selectedItems[i - 1]);
            }


        }
        private void buttonClearSource_Click(object sender, EventArgs e)
        {
            listBoxSource.Items.Clear();
        }

        private void buttonClearProgress_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
        }

        private void buttonBrowse1_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.Show("Browse for source files", buttonBrowse1);
        }

        private void buttonBrowse2_MouseEnter(object sender, EventArgs e)
        {
            toolTip2.Show("Browse for destination directory", buttonBrowse2);
        }

        private void buttonClearProgress_MouseEnter(object sender, EventArgs e)
        {
            toolTip3.Show("Clear progress bar", buttonClearProgress);
        }

        private void buttonClearSource_MouseEnter(object sender, EventArgs e)
        {
            toolTip4.Show("Clear all source files from source box", buttonClearSource);
        }

        private void buttonRemove_MouseEnter(object sender, EventArgs e)
        {
            toolTip5.Show("Remove selected files from source box", buttonRemove);
        }

        private void buttonBrowse1_MouseLeave(object sender, EventArgs e)
        {
            toolTip1.Hide(buttonBrowse1);
        }

        private void buttonBrowse2_MouseLeave(object sender, EventArgs e)
        {
            toolTip2.Hide(buttonBrowse2);
        }

        private void buttonRemove_MouseLeave(object sender, EventArgs e)
        {
            toolTip5.Hide(buttonRemove);
        }

        private void buttonClearSource_MouseLeave(object sender, EventArgs e)
        {
            toolTip4.Hide(buttonClearSource);
        }

        private void buttonClearProgress_MouseLeave(object sender, EventArgs e)
        {
            toolTip3.Hide(buttonClearProgress);
        }
    }
}
