﻿using System;

namespace CopyTool
{
    partial class CopyTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonBrowse1 = new System.Windows.Forms.Button();
            this.buttonBrowse2 = new System.Windows.Forms.Button();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.textBoxDestination = new System.Windows.Forms.TextBox();
            this.labelSource = new System.Windows.Forms.Label();
            this.labelDestination = new System.Windows.Forms.Label();
            this.labelProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.listBoxSource = new System.Windows.Forms.ListBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonClearSource = new System.Windows.Forms.Button();
            this.buttonClearProgress = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip5 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // buttonBrowse1
            // 
            this.buttonBrowse1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonBrowse1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonBrowse1.Location = new System.Drawing.Point(541, 37);
            this.buttonBrowse1.Name = "buttonBrowse1";
            this.buttonBrowse1.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowse1.TabIndex = 0;
            this.buttonBrowse1.Text = "Browse";
            this.buttonBrowse1.UseVisualStyleBackColor = true;
            this.buttonBrowse1.Click += new System.EventHandler(this.buttonBrowse1_Click);
            this.buttonBrowse1.MouseEnter += new System.EventHandler(this.buttonBrowse1_MouseEnter);
            this.buttonBrowse1.MouseLeave += new System.EventHandler(this.buttonBrowse1_MouseLeave);
            // 
            // buttonBrowse2
            // 
            this.buttonBrowse2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonBrowse2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonBrowse2.Location = new System.Drawing.Point(541, 95);
            this.buttonBrowse2.Name = "buttonBrowse2";
            this.buttonBrowse2.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowse2.TabIndex = 1;
            this.buttonBrowse2.Text = "Browse";
            this.buttonBrowse2.UseVisualStyleBackColor = true;
            this.buttonBrowse2.Click += new System.EventHandler(this.buttonBrowse2_Click);
            this.buttonBrowse2.MouseEnter += new System.EventHandler(this.buttonBrowse2_MouseEnter);
            this.buttonBrowse2.MouseLeave += new System.EventHandler(this.buttonBrowse2_MouseLeave);
            // 
            // buttonCopy
            // 
            this.buttonCopy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonCopy.Location = new System.Drawing.Point(541, 139);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(75, 23);
            this.buttonCopy.TabIndex = 2;
            this.buttonCopy.Text = "Copy";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // textBoxDestination
            // 
            this.textBoxDestination.AllowDrop = true;
            this.textBoxDestination.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxDestination.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxDestination.Location = new System.Drawing.Point(74, 95);
            this.textBoxDestination.Name = "textBoxDestination";
            this.textBoxDestination.Size = new System.Drawing.Size(461, 20);
            this.textBoxDestination.TabIndex = 4;
            this.textBoxDestination.DragDrop += new System.Windows.Forms.DragEventHandler(this.textBoxDestination_DragDrop);
            this.textBoxDestination.DragEnter += new System.Windows.Forms.DragEventHandler(this.textBoxDestination_DragEnter);
            // 
            // labelSource
            // 
            this.labelSource.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelSource.AutoSize = true;
            this.labelSource.Location = new System.Drawing.Point(24, 30);
            this.labelSource.Name = "labelSource";
            this.labelSource.Size = new System.Drawing.Size(44, 13);
            this.labelSource.TabIndex = 5;
            this.labelSource.Text = "Source:";
            // 
            // labelDestination
            // 
            this.labelDestination.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelDestination.AutoSize = true;
            this.labelDestination.Location = new System.Drawing.Point(5, 98);
            this.labelDestination.Name = "labelDestination";
            this.labelDestination.Size = new System.Drawing.Size(63, 13);
            this.labelDestination.TabIndex = 6;
            this.labelDestination.Text = "Destination:";
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(17, 139);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(51, 13);
            this.labelProgress.TabIndex = 7;
            this.labelProgress.Text = "Progress:";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar1.Location = new System.Drawing.Point(74, 138);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(461, 23);
            this.progressBar1.TabIndex = 8;
            // 
            // listBoxSource
            // 
            this.listBoxSource.AllowDrop = true;
            this.listBoxSource.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxSource.FormattingEnabled = true;
            this.listBoxSource.Location = new System.Drawing.Point(74, 30);
            this.listBoxSource.Name = "listBoxSource";
            this.listBoxSource.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxSource.Size = new System.Drawing.Size(461, 30);
            this.listBoxSource.TabIndex = 10;
            this.listBoxSource.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxSource_DragDrop);
            this.listBoxSource.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxSource_DragDrop);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRemove.Location = new System.Drawing.Point(379, 66);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonRemove.TabIndex = 11;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            this.buttonRemove.MouseEnter += new System.EventHandler(this.buttonRemove_MouseEnter);
            this.buttonRemove.MouseLeave += new System.EventHandler(this.buttonRemove_MouseLeave);
            // 
            // buttonClearSource
            // 
            this.buttonClearSource.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonClearSource.Location = new System.Drawing.Point(460, 66);
            this.buttonClearSource.Name = "buttonClearSource";
            this.buttonClearSource.Size = new System.Drawing.Size(75, 23);
            this.buttonClearSource.TabIndex = 12;
            this.buttonClearSource.Text = "Clear";
            this.buttonClearSource.UseVisualStyleBackColor = true;
            this.buttonClearSource.Click += new System.EventHandler(this.buttonClearSource_Click);
            this.buttonClearSource.MouseEnter += new System.EventHandler(this.buttonClearSource_MouseEnter);
            this.buttonClearSource.MouseLeave += new System.EventHandler(this.buttonClearSource_MouseLeave);
            // 
            // buttonClearProgress
            // 
            this.buttonClearProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonClearProgress.Location = new System.Drawing.Point(460, 167);
            this.buttonClearProgress.Name = "buttonClearProgress";
            this.buttonClearProgress.Size = new System.Drawing.Size(75, 23);
            this.buttonClearProgress.TabIndex = 13;
            this.buttonClearProgress.Text = "Clear";
            this.buttonClearProgress.UseVisualStyleBackColor = true;
            this.buttonClearProgress.Click += new System.EventHandler(this.buttonClearProgress_Click);
            this.buttonClearProgress.MouseEnter += new System.EventHandler(this.buttonClearProgress_MouseEnter);
            this.buttonClearProgress.MouseLeave += new System.EventHandler(this.buttonClearProgress_MouseLeave);
            // 
            // CopyTool
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(631, 202);
            this.Controls.Add(this.buttonClearProgress);
            this.Controls.Add(this.buttonClearSource);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.listBoxSource);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.labelDestination);
            this.Controls.Add(this.labelSource);
            this.Controls.Add(this.textBoxDestination);
            this.Controls.Add(this.buttonCopy);
            this.Controls.Add(this.buttonBrowse2);
            this.Controls.Add(this.buttonBrowse1);
            this.MinimumSize = new System.Drawing.Size(647, 241);
            this.Name = "CopyTool";
            this.Text = "Copy Tool";
            this.Load += new System.EventHandler(this.CopyTool_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonClear_Click_1(object sender, EventArgs e)
        {

        }

        #endregion

        private System.Windows.Forms.Button buttonBrowse1;
        private System.Windows.Forms.Button buttonBrowse2;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.TextBox textBoxDestination;
        private System.Windows.Forms.Label labelSource;
        private System.Windows.Forms.Label labelDestination;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ListBox listBoxSource;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonClearSource;
        private System.Windows.Forms.Button buttonClearProgress;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolTip toolTip5;
    }
}

